/*
 * This file is part of system-settings
 *
 * Copyright (C) 2013-2014 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import SystemSettings 1.0
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import Lomiri.Connectivity 1.0
import Lomiri.SystemSettings.Update 1.0
import "i18nd.js" as I18nd

ItemPage {
    id: root
    objectName: "configurationPage"
    title: I18nd.tr("Auto download")

    ListItem.ItemSelector {
        id: upgradePolicySelector
        anchors.top: root.header.bottom
        expanded: true
        text: I18nd.tr ("Download future updates automatically:")
        model: downloadSelector
        delegate: selectorDelegate
        selectedIndex: SystemImage.downloadMode
        onSelectedIndexChanged: SystemImage.downloadMode = selectedIndex
        Component.onCompleted: selectedIndex = SystemImage.downloadMode
    }
    Component {
        id: selectorDelegate
        OptionSelectorDelegate { text: name; subText: description; }
    }

    ListModel {
        id: downloadSelector
        /* Workaround toolkit limitation, translated values can't be used
           to build listitem, so we don't it from js
           see https://bugreports.qt-project.org/browse/QTBUG-20631 */
        Component.onCompleted: {
            insert(0, { name: I18nd.tr("Never"), description: "" })
            insert(1, { name: I18nd.tr("When on WiFi"), description: "" })
            if (NetworkingStatus.modemAvailable) {
                insert(2, { name: I18nd.tr("On any data connection"),
                       description: I18nd.tr("Data charges may apply.") })
            }
        }
    }
}
