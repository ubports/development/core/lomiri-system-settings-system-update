/*
 * This file is part of system-settings
 *
 * Copyright (C) 2017 The UBports project
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Marius Gripsgard <marius@ubports.com>
 */

import QtQuick 2.4
import SystemSettings 1.0
import Lomiri.Components 1.3
import SystemSettings.ListItems 1.0 as SettingsListItems
import Lomiri.Connectivity 1.0
import Lomiri.SystemSettings.Update 1.0
import "i18nd.js" as I18nd
import "ChUtils.js" as ChUtils

ItemPage {
    id: updatePage
    objectName: "updateSettingsPage"
    title: I18nd.tr("Update settings")
    flickable: pageFlickable

    Flickable {
        id: pageFlickable
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: contentItem.childrenRect.height

        // Only allow flicking if the content doesn't fit on the page
        boundsBehavior: (contentHeight > updatePage.height) ?
                         Flickable.DragAndOvershootBounds : Flickable.StopAtBounds


        Column {
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }

            SettingsListItems.SingleValueProgression {
                objectName: "configuration"
                text: I18nd.tr("Auto download")
                value: {
                    if (SystemImage.downloadMode === 0)
                        return I18nd.tr("Never")
                    else if (SystemImage.downloadMode === 1)
                        return I18nd.tr("On Wi-Fi")
                    else if (SystemImage.downloadMode === 2)
                        return I18nd.tr("Always")
                    else
                        return I18nd.tr("Unknown")
                }
                onClicked: pageStack.addPageToNextColumn(updatePage, Qt.resolvedUrl("Configuration.qml"))
            }
            SettingsListItems.SingleValueProgression {
                id: channelItem
                objectName: "channel"

                text: I18nd.tr("Channels")
                visible: value.length < 2 ? false : true

                function populate() {
                    const channel = SystemImage.getSwitchChannel()
                    channelItem.value = ChUtils.parseChannel(channel).prettyName
                }

                onClicked: {
                    var incubator = pageStack.addPageToNextColumn(updatePage, Qt.resolvedUrl("ChannelSettings.qml"))
                    if (incubator.status === Component.Ready) {
                        incubator.object.channelChanged.connect(channelItem.populate);
                    } else {
                        incubator.onStatusChanged = function(status) {
                            if (status === Component.Ready) {
                                incubator.object.channelChanged.connect(channelItem.populate);
                            }
                        }
                    }
                }

                Component.onCompleted: populate()
            }
            // Disabled pending further testing
            // SettingsListItems.SingleValueProgression {
            //     objectName: "firmwareUpdate"
            //     text: I18nd.tr("Firmware update")
            //     visible: SystemImage.supportsFirmwareUpdate()
            //     onClicked: pageStack.addPageToNextColumn(updatePage, Qt.resolvedUrl("FirmwareUpdate.qml"))
            // }
            SettingsListItems.SingleValueProgression {
                objectName: "appReinstall"
                text: I18nd.tr("Reinstall all apps")
                onClicked: pageStack.addPageToNextColumn(updatePage, Qt.resolvedUrl("ReinstallAllApps.qml"))
            }
        }
    }
}
