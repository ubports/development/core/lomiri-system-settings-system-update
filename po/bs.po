# Bosnian translation for lomiri-system-settings-system-update
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-24 13:53+0000\n"
"PO-Revision-Date: 2013-07-02 11:38+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#: plugins/system-update/UpdateSettings.qml:33
#: plugins/system-update/PageComponent.qml:44
#, fuzzy
msgid "Update settings"
msgstr "Sistematske postavke"

#: plugins/system-update/UpdateSettings.qml:56
#: plugins/system-update/Configuration.qml:30
msgid "Auto download"
msgstr "automatsko skidanje podataka"

#: plugins/system-update/UpdateSettings.qml:59
#: plugins/system-update/Configuration.qml:54
msgid "Never"
msgstr "Nikada"

#: plugins/system-update/UpdateSettings.qml:61
#, fuzzy
#| msgid "When on WiFi"
msgid "On Wi-Fi"
msgstr "Kada je WiFi uključen"

#: plugins/system-update/UpdateSettings.qml:63
msgid "Always"
msgstr "Uvijek"

#: plugins/system-update/UpdateSettings.qml:65
msgid "Unknown"
msgstr "Nepoznato"

#: plugins/system-update/UpdateSettings.qml:73
#, fuzzy
msgid "Channels"
msgstr "Izmijena"

#: plugins/system-update/UpdateSettings.qml:105
#: plugins/system-update/ReinstallAllApps.qml:37
#: plugins/system-update/ReinstallAllApps.qml:96
msgid "Reinstall all apps"
msgstr ""

#: plugins/system-update/InstallationFailed.qml:26
#: plugins/system-update/DownloadHandler.qml:175
msgid "Installation failed"
msgstr "Instalacija nije uspjela"

#: plugins/system-update/InstallationFailed.qml:29
msgid "OK"
msgstr "Uredu"

#: plugins/system-update/ReinstallAllApps.qml:86
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""

#: plugins/system-update/ReinstallAllApps.qml:150
#: plugins/system-update/PageComponent.qml:223
msgid "Connect to the Internet to check for updates."
msgstr ""

#: plugins/system-update/ReinstallAllApps.qml:152
#: plugins/system-update/PageComponent.qml:225
msgid "Software is up to date"
msgstr ""

#: plugins/system-update/ReinstallAllApps.qml:155
#: plugins/system-update/PageComponent.qml:228
msgid "The update server is not responding. Try again later."
msgstr ""

#: plugins/system-update/ReinstallAllApps.qml:164
#: plugins/system-update/PageComponent.qml:237
#, fuzzy
msgid "Updates Available"
msgstr "Dostupna ažuriranja"

#: plugins/system-update/ChannelSettings.qml:34
msgid "Channel settings"
msgstr ""

#: plugins/system-update/ChannelSettings.qml:109
msgid "Fetching channels"
msgstr ""

#: plugins/system-update/ChannelSettings.qml:118
msgid "Channel to get updates from:"
msgstr ""

#: plugins/system-update/ChannelSettings.qml:134
msgid "Switching channel"
msgstr ""

#: plugins/system-update/UpdateDelegate.qml:120
msgid "Retry"
msgstr "Ponovi"

#: plugins/system-update/UpdateDelegate.qml:125
msgid "Update"
msgstr "Ažuriraj"

#: plugins/system-update/UpdateDelegate.qml:127
msgid "Download"
msgstr "Preuzmi"

#: plugins/system-update/UpdateDelegate.qml:133
msgid "Resume"
msgstr "Nastavi"

#: plugins/system-update/UpdateDelegate.qml:140
msgid "Pause"
msgstr "Pauza"

#: plugins/system-update/UpdateDelegate.qml:144
msgid "Install…"
msgstr ""

#: plugins/system-update/UpdateDelegate.qml:146
msgid "Install"
msgstr "Instalirajte"

#: plugins/system-update/UpdateDelegate.qml:150
msgid "Open"
msgstr ""

#: plugins/system-update/UpdateDelegate.qml:239
msgid "Installing"
msgstr "Instaliranje"

#: plugins/system-update/UpdateDelegate.qml:243
#, fuzzy
msgid "Paused"
msgstr "Pauza"

#: plugins/system-update/UpdateDelegate.qml:246
#, fuzzy
msgid "Waiting to download"
msgstr "automatsko skidanje podataka"

#: plugins/system-update/UpdateDelegate.qml:249
msgid "Downloading"
msgstr "Preuzimanje"

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: plugins/system-update/UpdateDelegate.qml:285
#, qt-format
msgid "%1 of %2"
msgstr ""

#: plugins/system-update/UpdateDelegate.qml:289
#, fuzzy
msgid "Downloaded"
msgstr "Preuzmi"

#: plugins/system-update/UpdateDelegate.qml:292
msgid "Installed"
msgstr "Instalirano"

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: plugins/system-update/UpdateDelegate.qml:297
#, fuzzy, qt-format
msgid "Updated %1"
msgstr "Ažuriraj"

#: plugins/system-update/UpdateDelegate.qml:321
#, fuzzy
msgid "Update failed"
msgstr "Dostupna ažuriranja"

#: plugins/system-update/ImageUpdatePrompt.qml:31
msgid "Update System"
msgstr "Ažuriraj sistem"

#: plugins/system-update/ImageUpdatePrompt.qml:33
msgid "The device needs to restart to install the system update."
msgstr ""

#: plugins/system-update/ImageUpdatePrompt.qml:34
msgid "Connect the device to power before installing the system update."
msgstr ""

#: plugins/system-update/ImageUpdatePrompt.qml:39
msgid "Restart & Install"
msgstr ""

#: plugins/system-update/ImageUpdatePrompt.qml:49
#: plugins/system-update/PageComponent.qml:125
msgid "Cancel"
msgstr "Otkaži"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugins/system-update/PageComponent.qml:38 plugin-strings.generated.js:2
msgid "Updates"
msgstr "Ažuriranja"

#: plugins/system-update/PageComponent.qml:52
#: plugins/system-update/PageComponent.qml:111
#, fuzzy
msgid "Clear updates"
msgstr "Provjeri ažuriranja"

#: plugins/system-update/PageComponent.qml:112
#, fuzzy
msgid "Clear the update list?"
msgstr "Provjeri ažuriranja"

#: plugins/system-update/PageComponent.qml:129
msgid "Clear"
msgstr ""

#: plugins/system-update/PageComponent.qml:371
#, fuzzy
msgid "Recent updates"
msgstr "Provjeri ažuriranja"

#: plugins/system-update/GlobalUpdateControls.qml:85
msgid "Checking for updates…"
msgstr "Provjera nadogradnji..."

#: plugins/system-update/GlobalUpdateControls.qml:90
msgid "Stop"
msgstr ""

#. TRANSLATORS: %1 is number of software updates available.
#: plugins/system-update/GlobalUpdateControls.qml:117
#, fuzzy, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "Dostupna ažuriranja"
msgstr[1] "Dostupna ažuriranja"
msgstr[2] "Dostupna ažuriranja"

#: plugins/system-update/GlobalUpdateControls.qml:127
#, fuzzy
msgid "Update all…"
msgstr "Dostupna ažuriranja"

#: plugins/system-update/GlobalUpdateControls.qml:129
#, fuzzy
msgid "Update all"
msgstr "Ažuriraj"

#: plugins/system-update/FirmwareUpdate.qml:37
#: plugins/system-update/FirmwareUpdate.qml:158
msgid "Firmware Update"
msgstr ""

#: plugins/system-update/FirmwareUpdate.qml:134
#, fuzzy
msgid "There is a firmware update available!"
msgstr "Dostupna ažuriranja"

#: plugins/system-update/FirmwareUpdate.qml:135
msgid "Firmware is up to date!"
msgstr ""

#: plugins/system-update/FirmwareUpdate.qml:170
msgid "The device will restart automatically after installing is done."
msgstr ""

#: plugins/system-update/FirmwareUpdate.qml:186
#, fuzzy
msgid "Install and restart now"
msgstr "Instaliranje i resetovanje"

#: plugins/system-update/FirmwareUpdate.qml:229
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""

#: plugins/system-update/FirmwareUpdate.qml:236
#, fuzzy
msgid "Checking for firmware update"
msgstr "Provjera nadogradnji..."

#: plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr "Automatsko buduće ažuriranje najnovijih informacija"

#: plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr "Kada je WiFi uključen"

#: plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr "Na bilo koju podatkovnu konekciju"

#: plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr ""

#: plugins/system-update/ChangelogExpander.qml:43
#, fuzzy, qt-format
msgid "Version %1"
msgstr "Izdanje: "

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:4
msgid "system"
msgstr "sistem"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:6
msgid "software"
msgstr "softver"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:8
msgid "update"
msgstr "ažuriraj"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:10
msgid "apps"
msgstr "aplikacije"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:12
#, fuzzy
msgid "application"
msgstr "pisanje velikih slova"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:14
msgid "automatic"
msgstr "automatski"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:16
msgid "download"
msgstr "preuzimanje"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:18
msgid "upgrade"
msgstr "nadogradi"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: plugin-strings.generated.js:20
msgid "click"
msgstr "klik"

#. TRANSLATORS: This is a keyword or name for the update-notification plugin which is used while searching
#: plugin-strings.generated.js:22
#, fuzzy
msgid "Updates available"
msgstr "Dostupna ažuriranja"

#, fuzzy
#~ msgid "Development"
#~ msgstr "programer"

#, fuzzy
#~ msgid "Stable"
#~ msgstr "Tablet"
