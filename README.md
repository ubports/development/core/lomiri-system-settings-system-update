# Lomiri System Settings: System Update Plugin

## Ubuntu Touch's system update plugin for Lomiri system settings

This plugin provides the system update plugin to lomiri-system-settings.
The plugin contains the system update section, which handles both system
updates and (click) application updates.

This package also comes with the push helper for handling update notifications.

## i18n: Translating Lomiri System Settings' System Update Plugin into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-system-settings-system-update

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
