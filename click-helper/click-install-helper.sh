#!/bin/sh

if [ $# -ne 1 ]; then
    printf "usage: %s package\n" "$(basename $0)" >&2
    exit 1
fi
package_path="$(realpath "$1")"

reply="$(gdbus call --system --dest com.lomiri.click --object-path /com/lomiri/click --method com.lomiri.click.Install "${package_path}" 2>&1)"
status=$?
if [ $status -ne 0 ]; then
    printf "%s" "${reply}" >&2
fi
exit $status
